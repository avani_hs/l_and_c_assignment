import socket
import os
import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from _thread import *
from Util.constants import *
import threading
from datetime import datetime
from Database import dbinteraction as database
import json

class Server:

    def __init__(self):
        self.thread_count = 0

    def print_server_details(self):
        print("Server is waiting for clients to connect")
        print("Server is running at Port :"+str(PORT)+" and IP: "+str(HOST))

      
    def client_thread(self,connection,address):
        try:
            
            connection.send(str.encode("You are connect to the server: IP= "+str(HOST)+", Port= "+str(PORT)))
            connection.send(str.encode(str(address[1])))
            while True:
                topic=connection.recv(1024)
                topic=str(topic).replace("b'","")
                topic=topic.replace("'","")
                data=connection.recv(1024)
                data=json.loads(data)
                if(str(data['data']).lower()=='q'):
                    print("Client connection ended with Client:IP=" +address[0]+", Port="+str(address[1]))
                    connection.close()
                    self.thread_count-=1
                    break
                if not data:
                    break
                db=database.DbConnection()
                db.create_connection(str(address[0]),str(address[1]),data['data'],data['create-time'],data['expire-time'],str(topic),'Publisher')
                reply="Echo data: " + data['data']
                connection.sendall(str.encode(reply))
            connection.close()
        except:
            print("Client disconnected IP=" +address[0]+", Port="+str(address[1]))
            connection.close()
            self.thread_count-=1 
    
    def main(self):
        
        serversocket = socket.socket()
        try:
            serversocket.bind((HOST,PORT))
        except socket.error as e:
            print(str(e))
        
        Server.print_server_details(self)
        serversocket.listen(5)
        
        while True:
            client,address=serversocket.accept()
            client_IP = str(address[0])
            client_port = str(address[1])
            self.thread_count+=1
            print("New Thread created......Total Number of Working Thread: "+str(self.thread_count))
            print("Connected to client: IP=" +address[0]+", Port="+str(address[1]))
            start_new_thread(self.client_thread,(client,address))
            
        serversocket.close()



            