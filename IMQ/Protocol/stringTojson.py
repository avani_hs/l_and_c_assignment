import json
import datetime
class StringToJson:
    
    def __init__(self):
        print('json object is created')

    def convet_to_json(self,message):
        create_time = str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M")) 
        expire_time = datetime.datetime.now() + datetime.timedelta(days=2)
        expire_time = str(expire_time.strftime("%d-%m-%Y %H:%M"))
        string_to_json= str('{"create-time": "'+create_time+'","expire-time": "'+expire_time+'","data": "'+message+'"}')

        json_string= json.loads(string_to_json)
        return json_string,json_string["create-time"],json_string["expire-time"]
        

