import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from dataclasses import dataclass
from Protocol.protocol import *


@dataclass
class Request(Protocol):
    request_type: str = "POST"
