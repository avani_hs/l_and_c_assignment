import os
import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from Database import dbinteraction as database
from Util import queueOperations
from Client.Service import publisher as pb 


class Publisher:
    def __init__(self):
        print('Hello Publisher')
        self.is_connected_to_topic=False
    
    def set_topic(self,client_host,client_port,socket):
        queue=queueOperations.Queue()
        topic_list=queue.create_topic_queue()
        while(True):
            print("Available list of topics:")
            print(topic_list)
            topic=input('Enter topic name:')
            for topics in topic_list:
                if(topic==topics):
                    publish=pb.Service()
                    publish.main(topic,client_host,client_port,socket)
                    self.is_connected_to_topic=True
            if(self.is_connected_to_topic==False):
                add_topic=input("Topic which you entered is not in list, press 1 to add topic to list, any other key to view list: ")
                if(add_topic=='1'):
                    db=database.DbConnection()
                    db.insert_topic(topic)
                    publish=pb.Service()
                    publish.main(topic,client_host,client_port,socket)
                    break        
            else:
                break
 
        