import os
import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from Util import queueOperations

class Service:
    
    def __init__(self):
        print('Subscriber service is running')
    
    def get_data_from_queue(self,topic):
        queue=queueOperations.Queue()
        topic_list=queue.create_topic_queue()
        queue.load_data_to_queue(topic_list)
        topic_queue=queue.get_queue_by_topic(topic)
        print('Connected to Queue with topic:',topic)
        while (True):
            if (topic_queue.empty()):
                print("Queue is empty") 
                print("Disconnected from server")
                break 
            print(topic_queue.get())
            next_message=input('Press 1 to the fetch next message, any other key to end the connection:')
            if(next_message!='1'):
                break
                
            
            
