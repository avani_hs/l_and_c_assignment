import os
import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from Util import queueOperations as queue
from Protocol import stringTojson as parcer
from Database import dbinteraction as database
import json

class Service:

    def __init__(self):
        print('Publisher service is running')
        
    def generate_data_to_publish(self,topic):
        data=input('Enter data to publish(Press q/Q to quit):')
        json_obj=parcer.StringToJson()
        data,create_time,expire_time=json_obj.convet_to_json(data)
        return data
        
    def publish_data(self,topic,data,client_host,client_port,socket):
        data=json.dumps(data)
        socket.sendall(bytes(topic,'utf-8'))
        socket.sendall(bytes(data,'utf-8'))
        
    def main(self,topic,client_host,client_port,socket):
        while (True):
            data= Service.generate_data_to_publish(self,topic)
            if((str(data['data']).lower())=='q'):
                    Service.publish_data(self,topic,data,client_host,client_port,socket)
                    print("Disconnected from server")
                    break
            Service.publish_data(self,topic,data,client_host,client_port,socket)
            print("Data:"+str(data['data']) +"; has been sent to server")
            received_data = socket.recv(1024)
            print("Received data:"+str(received_data)[1:])
        

