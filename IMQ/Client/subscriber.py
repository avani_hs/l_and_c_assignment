import os
import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from Util import queueOperations
from Client.Service import subscriber 

class Subscriber:

    def __init__(self):
        print('Hello Subscriber')
    
    def connect_to_topic(self):
        queue=queueOperations.Queue()
        topic_list=queue.create_topic_queue()
        print (topic_list)
        while(True):
            topic=input('Enter valid topic name from above listed topics: ')
            if(topic in topic_list):
                break
            print('Invalid input')
        return topic
    
    def get_messages(self):
        topic=Subscriber.connect_to_topic(self)
        subscribe=subscriber.Service()
        subscribe.get_data_from_queue(topic)
    
