import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
import socket
from  Protocol import stringTojson
import json
import Client.publisher as publisher
import Client.subscriber as subscriber

class Client:
    
    def __init__(self):
        self.server_HOST= '127.0.0.1'
        self.server_PORT=65432
        
    def get_client_details(self,client_host,client_port,socket):
        while (True):
            client_type = input("Press '1' to publish data, press '2' to subscribe data: ");
            if (client_type=='1'):
                client=publisher.Publisher()
                client.set_topic(client_host,client_port,socket)
                break
            
            elif(client_type=='2'):
                client=subscriber.Subscriber()
                client.get_messages()
                break
            else:
                print('Invalid input')

    def connect_to_server(self):
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as Socket:
                Socket.connect((self.server_HOST, self.server_PORT))
                received_data = Socket.recv(1024)    
                print("Connection Details:"+str(received_data)[1:])
                client_port=Socket.recv(1024)
                client_port=str(client_port).replace("b'","")
                client_port=client_port.replace("'","")
                client_port=int(client_port)
                client_host='127.0.0.1'
                Client.get_client_details(self,client_host,client_port,Socket)
        except:
            print("Server is not running or refused to acccept the connection request.")
c = Client()
c.connect_to_server()
