import json
import datetime
class StringToJson:
    
    def __init__(self):
        print('json object is created')

    def convet_to_json(self,message):
        createTime = str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M")) 
        expireTime = datetime.datetime.now() + datetime.timedelta(days=2)
        expireTime = str(expireTime.strftime("%d-%m-%Y %H:%M"))
        stringTojson= str('{"create-time": "'+createTime+'","expire-time": "'+expireTime+'","data": "'+message+'"}')

        jsonString= json.loads(stringTojson)
        return jsonString,jsonString["create-time"],jsonString["expire-time"]
        

