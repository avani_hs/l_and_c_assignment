import sys
sys.path.append('D:\l_and_c_assignment\IMQ')
from Database import dbinteraction as database
import queue

class Queue:
    def __init__(self):
        self.topic_list=[]
        self.queue=dict()
    
    def format_input_data(self,input_data):
        input_data=input_data.replace(',','')
        input_data=input_data.replace(' ','')
        input_data=input_data.replace(')(',',')
        input_data=input_data.replace('(','')
        input_data=input_data.replace(')','')
        input_data=input_data.replace("'","")
        input_data=list(str.split(input_data,','))
        return input_data

    def create_topic_queue(self):
        db=database.DbConnection()
        topic_list=db.get_topic_list()
        topic_list=Queue.format_input_data(self,topic_list)
        return topic_list
        
    def load_messages(self,topic):
        db=database.DbConnection()
        messages=db.get_messages(topic)
        messages=Queue.format_input_data(self,messages)
        return(messages)
    
    def load_data_to_queue(self,topic_list):
        for topic in topic_list:
            self.queue.setdefault(topic,Queue.load_messages(self,topic))
            
    def get_queue_by_topic(self,topic):
        topic_queue = queue.Queue()
        for message in self.queue[topic]:
            topic_queue.put(message)
        return topic_queue
        
    def load_dead_letter_queue(self):
        dead_queue=queue.Queue()
        db=database.DbConnection()
        dead_messages=db.get_dead_messages()
        dead_messages=Queue.format_input_data(self,dead_messages)
        print('Dead messages are:', dead_messages)

        
