import unittest
import os
import sys
sys.path.append('D:\l_and_c_assignment\IMQ')

from Util import queueOperations

class Test(unittest.TestCase):
    
    def test1_format_input_data_positive(self):
        input_data="('test',)('test',)"
        test_obj=queueOperations.Queue()
        output_data=test_obj.format_input_data(input_data)
        expected_value=['test', 'test']
        self.assertEqual(output_data, expected_value)
    
    def test2_format_input_data_negative(self):
        input_data="('test',)('test',)"
        test_obj=queueOperations.Queue()
        output_data=test_obj.format_input_data(input_data)
        expected_value=['test,test']
        self.assertNotEqual(output_data, expected_value)

if __name__ == '__main__':
    unittest.main()
    