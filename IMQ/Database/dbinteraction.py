import pyodbc 
from datetime import datetime
import logging

class DbConnection:

    def __init__(self):
        self.server = "."
        self.database = "GraduationProjectMQS"
        self.user_name = "MQS"
        self.password = "MQS_password"
    
    def write_data_to_database(self,ip,port,data,create_time,expire_time,cursor,topic,client_type):
        try:
            client_name = str(ip)+"_"+str(port)
            timestamp=datetime.now()
            timestamp=timestamp.strftime("%d/%m/%Y %H:%M:%S")
            cursor.execute("exec insertClientInfo_p @clientName=?,@creationDate=?,@lastInteractionDate=?,@interactionType=?",(str(client_name),timestamp,timestamp,str(client_type)))
            cursor.execute("exec insertClientData_p @clientIP=?,@clientPort=?,@topic=?,@clientData=?,@createTime=?,@expireTime=?,@clientName=?",str(ip),str(port),str(topic),str(data),str(create_time),str(expire_time),(str(client_name)))
           
        except RuntimeError as e:
            print("error accured while writing data to database",e)

    def get_topic_list(self):
        try:
            connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self.server+';DATABASE='+self.database+';UID='+self.user_name+';PWD='+ self.password)
            cursor = connection.cursor()
            cursor.execute("exec getTopicList_p")
            topic_list=''
            for row in cursor.fetchall():
                topic_list=topic_list+' '+str(row)
            connection.commit()
            return topic_list
        except RuntimeError as e:
           print("Database connection error occured: Client data is not inserted into database")
           print("ErrorMessage: "+e)
   
    def get_messages(self,topic):
        try:    
            connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self.server+';DATABASE='+self.database+';UID='+self.user_name+';PWD='+ self.password)
            cursor = connection.cursor()
            cursor.execute("exec getMessages_p @topicName=?",str(topic))
            messages=''
            for row in cursor.fetchall():
                messages=messages+' '+str(row)
            connection.commit()
            return messages
        except RuntimeError as e:
           print("Database connection error occured: Not able to load messages from database")
           print("ErrorMessage: "+e)

    def insert_topic(self,topic_name):
        try:
            connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self.server+';DATABASE='+self.database+';UID='+self.user_name+';PWD='+ self.password)
            cursor = connection.cursor()
            cursor.execute("exec insertTopic_p @topicName=?",str(topic_name))
            connection.commit()
        except RuntimeError as e:
            print("Database connection error occured: Topic_name is not inserted into database")
            print("ErrorMessage: "+e)
    
    def get_dead_messages(self):
        try:    
            connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self.server+';DATABASE='+self.database+';UID='+self.user_name+';PWD='+ self.password)
            cursor = connection.cursor()
            cursor.execute("exec getDeadMessages_p")
            messages=''
            for row in cursor.fetchall():
                messages=messages+' '+str(row)
            connection.commit()
            return messages
        except RuntimeError as e:
           print("Database connection error occured: Client data is not inserted into database")
           print("ErrorMessage: "+e)

    def create_connection(self,ip,port,data,create_time,expire_time,topic,client_type):
        try:
            connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self.server+';DATABASE='+self.database+';UID='+self.user_name+';PWD='+ self.password)
            logging.info("Connection is established with server and database"+ str(self.server)+str(self.database))
            cursor = connection.cursor()
            db=DbConnection() 
            db.write_data_to_database(ip,port,data,create_time,expire_time,cursor,topic,client_type)
            connection.commit() 
        except RuntimeError as e:
           print("Database connection error occured: Client data is not inserted into database")
           print("ErrorMessage: "+e)

    
