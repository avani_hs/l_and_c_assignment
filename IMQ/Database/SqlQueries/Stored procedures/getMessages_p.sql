USE [GraduationProjectMQS]
GO
/****** Object:  StoredProcedure [dbo].[getMessages_p]    Script Date: 3/30/2021 10:13:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[getMessages_p]
@topicName NVARCHAR(max)=NULL
AS 
BEGIN
	SET NOCOUNT ON
	SELECT [clientData]FROM [GraduationProjectMQS].[dbo].[clientData]
	WHERE LEN(clientData)<=100 AND [topicId] =(SELECT ID FROM [GraduationProjectMQS].[dbo].[topics] WHERE [Name]=@topicName)
	AND (SELECT (CONVERT(DATETIME,[expireTime],103))) >=(SELECT CONVERT(DATETIME,getdate(),103)) ORDER BY [serialNo]
END

	
