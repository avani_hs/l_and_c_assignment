CREATE PROCEDURE insertClientData_p
 @clientIP NVARCHAR(20) = NULL,
 @ClientPort INT = NULL,
 @topic NVARCHAR(50)= NULL,
 @clientData NVARCHAR(500)= NULL,
 @createTime NVARCHAR(50)= NULL,
 @expireTime NVARCHAR(20) = NULL,
 @clientName NVARCHAR(MAX)= NULL
AS 
BEGIN
	INSERT INTO [GraduationProjectMQS].[dbo].[clientData] VALUES 
	((SELECT ID FROM [GraduationProjectMQS].[dbo].[clientInfo] WHERE clientName=@clientName)
	,@clientIP,@ClientPort,(SELECT TOP(1) [ID] FROM [GraduationProjectMQS].[dbo].[topics] WHERE [Name]= @topic),@clientData,@createTime,@expireTime)
END
