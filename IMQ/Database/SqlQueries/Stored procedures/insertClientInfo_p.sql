CREATE PROCEDURE insertClientInfo_p
@clientName NVARCHAR(MAX)= NULL,
@creationDate NVARCHAR(MAX)=NULL,
@lastInteractionDate NVARCHAR(MAX)=NULL,
@interactionType NVARCHAR(MAX) = NULL
	
AS
BEGIN
	IF NOT EXISTS (SELECT clientName FROM CLIENTINFO WHERE clientName=@clientName) 
	BEGIN 
	 INSERT INTO [dbo].[clientInfo] VALUES (@clientName,@creationDate,@lastInteractionDate,@interactionType); 
	END
	ELSE 
		UPDATE [dbo].[clientInfo] SET [lastInteractionDate]=@lastInteractionDate where clientName=@clientName
END
